**Kendo UI Grid Demo - Change Row Color On Load**

This sample project goes along with the blog post here: (https://www.adambumgardner.com)

Demonstrates how to change the background color of individual rows in a Kendo UI grid based on the data in one of the rows columns.