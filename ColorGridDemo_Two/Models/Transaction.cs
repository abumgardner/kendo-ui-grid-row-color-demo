﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ColorGridDemo_Two.Models
{
    public class Transaction
    {
        public string Description { get; set; }
        public bool deposit { get; set; }
        public decimal Amount { get; set; }
    }
}