﻿using ColorGridDemo_Two.Models;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ColorGridDemo_Two.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Kendo Grid Row Color Demo!";

            return View();
        }


        public ActionResult Transactions_Read([DataSourceRequest] DataSourceRequest request)
        {
            var transactions = new List<Transaction>()
            {
                new Transaction() { Description="Opening Deposit",  deposit=true, Amount=15000.00m },
                new Transaction() { Description="Apple Store - New MBP",  deposit=false, Amount=3200.00m },
                new Transaction() { Description="Trek Cycles",  deposit=false, Amount=1986.99m },
                new Transaction() { Description="Whole Foods",  deposit=false, Amount=145.34m }
            };

            return Json(transactions.ToDataSourceResult(request));
        }
    }
}
